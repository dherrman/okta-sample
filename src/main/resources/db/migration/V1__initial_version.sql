-- Create the user's table
CREATE TABLE users (
    id       INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    name     VARCHAR NOT NULL
);

-- Create the devices table
CREATE TABLE devices (
    id       INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    user_id  INT NOT NULL,
    name     VARCHAR NOT NULL,
    platform VARCHAR NOT NULL,
    managed  BOOLEAN NOT NULL,
    FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE INDEX idx_devices_name ON devices(name);
CREATE INDEX idx_devices_platform ON devices(platform);
CREATE INDEX idx_devices_managed ON devices(managed);
