INSERT INTO users (id, name) values (100, 'Joe Smith');
INSERT INTO users (id, name) values (110, 'Jessy Smith');
INSERT INTO users (id, name) values (120, 'Bob Smith');
INSERT INTO users (id, name) values (130, 'Eli Smith');

INSERT INTO devices (user_id, name, platform, managed) values (100, 'laptop-windows', 'WIN', true);
INSERT INTO devices (user_id, name, platform, managed) values (100, 'laptop-mac', 'MAC', false);
INSERT INTO devices (user_id, name, platform, managed) values (100, 'a-phone', 'ANDROID', true);

INSERT INTO devices (user_id, name, platform, managed) values (110, 'laptop-windows', 'WIN', false);
INSERT INTO devices (user_id, name, platform, managed) values (110, 'laptop-mac', 'MAC', false);
INSERT INTO devices (user_id, name, platform, managed) values (110, 'phone', 'IOS', true);

INSERT INTO devices (user_id, name, platform, managed) values (120, 'windows', 'WIN', false);
INSERT INTO devices (user_id, name, platform, managed) values (120, 'windows-2', 'WIN', true);

INSERT INTO devices (user_id, name, platform, managed) values (130, 'laptop-windows', 'WIN', false);
INSERT INTO devices (user_id, name, platform, managed) values (130, 'laptop-mac', 'MAC', false);
INSERT INTO devices (user_id, name, platform, managed) values (130, 'phone', 'IOS', false);
