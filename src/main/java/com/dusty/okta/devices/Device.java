package com.dusty.okta.devices;

import java.util.Objects;

public class Device {

  private int id;
  private User user;
  private PLATFORM platform;
  private String name;
  private boolean managed;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PLATFORM getPlatform() {
    return platform;
  }

  public void setPlatform(PLATFORM platform) {
    this.platform = platform;
  }

  public boolean isManaged() {
    return managed;
  }

  public void setManaged(boolean managed) {
    this.managed = managed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Device device = (Device) o;
    return id == device.id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "Device{" +
            "id=" + id +
            ", user=" + user +
            ", name='" + name + '\'' +
            ", platform=" + platform +
            ", managed=" + managed +
            '}';
  }
}
