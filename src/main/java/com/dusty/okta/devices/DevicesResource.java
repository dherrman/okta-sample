package com.dusty.okta.devices;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class DevicesResource {

  public static final String DEVICE_SELECT_SQL_PART = "SELECT d.id, d.name, d.platform, d.managed, u.id as user_id, u.name as user_name FROM devices as d left join users as u on d.user_id = u.id";
  private final NamedParameterJdbcTemplate jdbcTemplate;

  public DevicesResource(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
  }

  @GetMapping("/devices")
  public List<Device> getAllDevices() {
    return jdbcTemplate.query(
            DEVICE_SELECT_SQL_PART,
            new DeviceRowMapper()
    );
  }

  @GetMapping("/devices/{deviceID}")
  public List<Device> getDevice(@PathVariable int deviceID) {
    return jdbcTemplate.getJdbcTemplate().query(
            DEVICE_SELECT_SQL_PART + " WHERE d.id = ?",
            new DeviceRowMapper(),
            deviceID );
  }

  @GetMapping("/platforms")
  public PLATFORM[] getAllPlatforms() {
    return PLATFORM.values();
  }

  /**
   * Will return the top 5 full device names for the name fragment.
   * Used by the UI for type-ahead.
   * @param name
   * @return
   */
  @GetMapping("/devices/search/autocomplete")
  public List<String> getSearchAutocomplete(@RequestParam String name) {
    return jdbcTemplate.getJdbcTemplate().query(
            "SELECT distinct name FROM devices WHERE name like ? limit 5",
            (rs, rowNum) -> rs.getString("name"),
            name + "%");
  }

  @GetMapping("/devices/search")
  public List<Device> doSearch(@RequestParam Optional<String> name,
                               @RequestParam Optional<PLATFORM> platform,
                               @RequestParam Optional<Boolean> managed,
                               @RequestParam(defaultValue = "0") int offset,
                               @RequestParam(defaultValue = "10") int limit ) {

    Map<String,Object> params = new HashMap<>();
    StringBuilder query = new StringBuilder();
    if (name.isPresent()) {
      if (query.length() != 0) {
        query.append(" AND");
      }
      query.append(" d.name = :name");
      params.put("name", name.get());
    }
    if (platform.isPresent()) {
      if (query.length() != 0) {
        query.append(" AND");
      }
      query.append(" platform = :platform");
      params.put("platform", platform.get().name());
    }
    if (managed.isPresent()) {
      if (query.length() != 0) {
        query.append(" AND");
      }
      query.append(" managed = :managed");
      params.put("managed", managed.get());
    }
    StringBuilder sql = new StringBuilder(DEVICE_SELECT_SQL_PART);
    if (query.length() != 0) {
      sql.append(" WHERE ").append(query);
    }
    sql.append(" limit :limit");
    params.put("limit", limit);
    sql.append(" offset :offset");
    params.put("offset", offset);
    return jdbcTemplate.query(
            sql.toString(),
            params,
            new DeviceRowMapper());
  }

  private class DeviceRowMapper implements RowMapper<Device> {

    @Override
    public Device mapRow(ResultSet rs, int rowNum) throws SQLException {
      Device device = new Device();
      device.setId(rs.getInt("id"));
      device.setName(rs.getString("name"));
      device.setPlatform(PLATFORM.valueOf(rs.getString("platform")));
      device.setManaged(rs.getBoolean("managed"));

      User user = new User();
      user.setId(rs.getInt("user_id"));
      user.setName(rs.getString("user_name"));
      device.setUser(user);
      return device;
    }
  }
}
