package com.dusty.okta.devices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OktaDevicesApplication {

  public static void main(String[] args) {
    SpringApplication.run(OktaDevicesApplication.class, args);
  }

}
