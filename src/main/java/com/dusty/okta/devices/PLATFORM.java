package com.dusty.okta.devices;

public enum PLATFORM {
  WIN,
  MAC,
  IOS,
  ANDROID
}
