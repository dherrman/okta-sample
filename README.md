# OKTA Sample application

This was a quick sample application that Yang Chen asked me during the
interview today (11/3/2021).  As I was unable to get a fully production
quality working application, I asked him to allow me to do the question
as a "take home" test.  This is the result.

## The Task
Fastpass persists the following device signals in RDBMS:
name(string, genereted by agent, not unique), platform(WIN, MAC, IOS, ANDROID), managed(boolean)

Assume “User” table already exists, design table(s) to store these signals and support CRUD + search by signals.
Design and implement (assume production level code, apply best practice) a REST interface to support search by signals:  name (startWith, used to support typeahead), platform (1 of the 4 values), managed (true/false).

## Getting Started
This application requires Java 11 or higher.
To start that application, run the following command:
```./mvnw spring-boot:run```

## Port
The application listens to the local port "8080"

## OpenAPI documentation
There is a simple OpenAPI documentation page located at http://localhost:8080/swagger-ui.html

## DataBase
This simple application uses an in memory H2 database.  The database is
recreated on each startup and not persisted anywhere.  A small set of sample
data is loaded on startup.
